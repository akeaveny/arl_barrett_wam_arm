#! /usr/bin/env python
from __future__ import division

import sys
import copy

import numpy as np
from scipy.spatial.transform import Rotation as R

import rospy

import tf
import tf2_ros
import tf2_geometry_msgs
import geometry_msgs.msg

from geometry_msgs.msg import Pose, PoseStamped
from sensor_msgs.msg import JointState
from std_msgs.msg import Float64MultiArray, Header
from std_srvs.srv import Empty

from visualization_msgs.msg import Marker

#######################
#######################

class PoseEstimator():

    def __init__(self):

        # WAM JOINTS
        # - wam / base_yaw_joint
        # - wam / elbow_pitch_joint
        # - wam / palm_yaw_joint
        # - wam / shoulder_pitch_joint
        # - wam / shoulder_yaw_joint
        # - wam / wrist_pitch_joint
        # - wam / wrist_yaw_joint
        self.num_arm_joints = 7

        self.wam_observation = {
            'joint_states': {
                "position": [0] * self.num_arm_joints,
                "velocity": [0] * self.num_arm_joints,
                "effort": [0] * self.num_arm_joints,
            },
            'ZED': {
                "position": [0] * 3,
                "orientation": [0] * 4,
            },
            'Object': {
                "position": [0] * 3,
                "orientation": [0] * 4,
            },
        }

        # JointState Callbacks
        rospy.Subscriber("/wam/joint_states", JointState, self.get_zed_to_world_transform)

        # WAM LINKS
        #   - ground_plane::link
        #   - barret_wam_arm::wam/base_link
        #   - barret_wam_arm::wam/shoulder_yaw_link
        #   - barret_wam_arm::wam/shoulder_pitch_link
        #   - barret_wam_arm::wam/upper_arm_link
        #   - barret_wam_arm::wam/forearm_link
        #   - barret_wam_arm::wam/wrist_yaw_link
        #   - barret_wam_arm::wam/wrist_pitch_link
        #   - barret_wam_arm::wam/wrist_palm_link
        self.model_name = 'barret_wam_arm'
        self.base_link_frame = 'wam/base_link'
        self.forearm_link_frame = 'wam/forearm_link'
        self.camera_frame = 'zed_left_camera_frame'
        self.object_frame = 'object_frame'

        # ZED CAMERA
        self.offset_to_left_ZED_lens_x_axis = -87.5 / 1000 # zed height
        self.offset_to_left_ZED_lens_z_axis = 60 / 1000    # left lens
        self.offset_to_left_ZED_lens_y_axis = -150 / 1000  # TODO: GET A MEASUREMENT !!!

        self.zed_in_forearm_frame = PoseStamped()
        self.zed_in_forearm_frame.header.stamp = rospy.Time.now()
        self.zed_in_forearm_frame.header.frame_id = self.base_link_frame  # self.camera_frame or self.base_frame
        self.zed_in_forearm_frame.pose.position.x = self.offset_to_left_ZED_lens_x_axis
        self.zed_in_forearm_frame.pose.position.y = self.offset_to_left_ZED_lens_y_axis
        self.zed_in_forearm_frame.pose.position.z = self.offset_to_left_ZED_lens_z_axis
        self.zed_in_forearm_frame.pose.orientation.x = 0
        self.zed_in_forearm_frame.pose.orientation.y = 0
        self.zed_in_forearm_frame.pose.orientation.z = 0
        self.zed_in_forearm_frame.pose.orientation.w = 1

        # Transforms:
        self.tf_buffer = tf2_ros.Buffer() # tf2_ros.Buffer(rospy.Duration(1.0))
        self.listener = tf2_ros.TransformListener(self.tf_buffer)

        self.transform_broadcaster = tf2_ros.TransformBroadcaster()

        self.zed_left_camera_frame = geometry_msgs.msg.TransformStamped()
        self.zed_left_camera_frame.header.frame_id = self.base_link_frame
        self.zed_left_camera_frame.child_frame_id = self.camera_frame

        # DenseFusion
        self.sub_densefusion = rospy.Subscriber("/aruco_single/pose", PoseStamped, self.aff_densefusion_callback)

        self.object_in_world_frame = geometry_msgs.msg.TransformStamped()
        self.object_in_world_frame.header.frame_id = self.base_link_frame
        self.object_in_world_frame.child_frame_id = self.object_frame

    #######################
    #######################

    # def WAMJointStateCallback(self, data):
    #     # joint states
    #     self.wam_observation['joint_states']['position'] = np.array(data.position)
    #     self.wam_observation['joint_states']['velocity'] = np.array(data.velocity)
    #     self.wam_observation['joint_states']['effort'] = np.array(data.effort)
    #
    #     # link states
    #     self.get_zed_to_world_transform()

    #######################
    #######################

    def get_zed_to_world_transform(self, data):
        ''' zed_T_world = zed_T_forearm * forearm_T_world '''

        listener = tf.TransformListener()
        listener.waitForTransform(self.base_link_frame, self.forearm_link_frame, rospy.Time(), rospy.Duration(1.0))

        #######################
        # get transform
        #######################

        # forearm_T_world                                        target frame           source frame
        forearm_to_world = self.tf_buffer.lookup_transform(self.base_link_frame, self.forearm_link_frame, rospy.Time(0))
        # zed_T_world
        zed_to_world = tf2_geometry_msgs.do_transform_pose(self.zed_in_forearm_frame, forearm_to_world)

        x = zed_to_world.pose.position.x
        y = zed_to_world.pose.position.y
        z = zed_to_world.pose.position.z
        position = np.array([x, y, z])

        x = zed_to_world.pose.orientation.x
        y = zed_to_world.pose.orientation.y
        z = zed_to_world.pose.orientation.z
        w = zed_to_world.pose.orientation.w
        orientation = np.array([x, y, z, w])

        #######################
        # Camera coordinate system -> Right HANDED Z UP and X FORWARD
        #######################

        rotation_matrix = R.from_quat([orientation[0],
                                       orientation[1],
                                       orientation[2],
                                       orientation[3]]).as_dcm()

        # ccw rotation about the x axis
        rad = np.pi / 2
        ccw_rotation_x_axis = np.array([[1, 0, 0],
                                        [0, np.cos(rad), -np.sin(rad)],
                                        [0, np.sin(rad), np.cos(rad)],
                                        ])

        # ccw rotation about the y axis
        rad = np.pi / 2
        ccw_rotation_y_axis = np.array([[np.cos(rad), 0, np.sin(rad)],
                                        [0, 1, 0],
                                        [-np.sin(rad), 0, np.cos(rad)],
                                        ])

        # ccw rotation about the z axis
        rad = -np.pi / 2
        ccw_rotation_z_axis = np.array([[np.cos(rad), -np.sin(rad), 0],
                                        [np.sin(rad), np.cos(rad), 0],
                                        [0, 0, 1],
                                        ])

        zed_rotation_matrix = np.dot(ccw_rotation_z_axis, rotation_matrix)
        zed_rotation_matrix = np.dot(ccw_rotation_y_axis, zed_rotation_matrix)

        # zed_rotation_matrix = np.dot(ccw_rotation_y_axis, rotation_matrix)
        # zed_rotation_matrix = np.dot(ccw_rotation_z_axis, zed_rotation_matrix)

        zed_orientation = R.from_dcm(zed_rotation_matrix).as_quat()

        self.wam_observation['ZED']['position'] = position
        self.wam_observation['ZED']['orientation'] = zed_orientation

        #######################
        # publish transform
        #######################

        self.zed_left_camera_frame.transform.translation.x = self.wam_observation['ZED']['position'][0]
        self.zed_left_camera_frame.transform.translation.y = self.wam_observation['ZED']['position'][1]
        self.zed_left_camera_frame.transform.translation.z = self.wam_observation['ZED']['position'][2]
        self.zed_left_camera_frame.transform.rotation.x = self.wam_observation['ZED']['orientation'][0]
        self.zed_left_camera_frame.transform.rotation.y = self.wam_observation['ZED']['orientation'][1]
        self.zed_left_camera_frame.transform.rotation.z = self.wam_observation['ZED']['orientation'][2]
        self.zed_left_camera_frame.transform.rotation.w = self.wam_observation['ZED']['orientation'][3]

        self.zed_left_camera_frame.header.stamp = rospy.Time.now()
        self.transform_broadcaster.sendTransform(self.zed_left_camera_frame)

    #######################
    #######################

    def aff_densefusion_callback(self, object_in_zed_frame):

        listener = tf.TransformListener()
        listener.waitForTransform(self.camera_frame, self.base_link_frame, rospy.Time(), rospy.Duration(1.0))

        #######################
        # get pose data
        #######################

        x = object_in_zed_frame.pose.position.x
        y = object_in_zed_frame.pose.position.y
        z = object_in_zed_frame.pose.position.z
        position = np.array([x, y, z])

        x = object_in_zed_frame.pose.orientation.x
        y = object_in_zed_frame.pose.orientation.y
        z = object_in_zed_frame.pose.orientation.z
        w = object_in_zed_frame.pose.orientation.w
        orientation = np.array([x, y, z, w])

        #######################
        # transform data
        #######################
        ''' object_T_world = object_T_zed * zed_T_world '''

        # zed_T_world                                       target frame,       source frame
        zed_to_world = self.tf_buffer.lookup_transform(self.base_link_frame, self.camera_frame, rospy.Time(0))
        # object_T_world
        object_to_world = tf2_geometry_msgs.do_transform_pose(object_in_zed_frame, zed_to_world)

        x = object_to_world.pose.position.x
        y = object_to_world.pose.position.y
        z = object_to_world.pose.position.z
        position = np.array([x, y, z])

        x = object_to_world.pose.orientation.x
        y = object_to_world.pose.orientation.y
        z = object_to_world.pose.orientation.z
        w = object_to_world.pose.orientation.w
        orientation = np.array([x, y, z, w])

        self.wam_observation['Object']['position'] = position
        self.wam_observation['Object']['orientation'] = orientation

        #######################
        # publish transform
        #######################

        self.object_in_world_frame.transform.translation.x = position[0]
        self.object_in_world_frame.transform.translation.y = position[1]
        self.object_in_world_frame.transform.translation.z = position[2]
        self.object_in_world_frame.transform.rotation.x = orientation[0]
        self.object_in_world_frame.transform.rotation.y = orientation[1]
        self.object_in_world_frame.transform.rotation.z = orientation[2]
        self.object_in_world_frame.transform.rotation.w = orientation[3]

        self.object_in_world_frame.header.stamp = rospy.Time.now()
        self.transform_broadcaster.sendTransform(self.object_in_world_frame)

    #######################
    #######################

def main():

    rospy.init_node('densefusion_to_moveit', anonymous=True)
    PoseEstimator()
    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        rate.sleep()
    # try:
    #     rospy.spin()
    # except KeyboardInterrupt:
    #     print ('Shutting down Moveit')

if __name__ == '__main__':
    main()
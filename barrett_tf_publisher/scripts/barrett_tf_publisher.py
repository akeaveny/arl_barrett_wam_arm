#! /usr/bin/env python
from __future__ import division

import sys
import copy

import numpy as np
from scipy.spatial.transform import Rotation as R

import rospy

import tf
import tf2_ros
import tf2_geometry_msgs
import geometry_msgs.msg

from gazebo_msgs.msg import ModelState, LinkStates, ContactsState
from gazebo_msgs.srv import GetModelState, SetModelState, GetLinkState, SetLinkState
from gazebo_msgs.srv import SetModelConfiguration, SetModelConfigurationRequest

from geometry_msgs.msg import Pose, PoseStamped
from sensor_msgs.msg import JointState
from std_msgs.msg import Float64MultiArray, Header
from std_srvs.srv import Empty

import std_msgs.msg
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2 as pcl2

from trac_ik_python.trac_ik import IK

#######################
#######################

class TFPublisher():

    def __init__(self):

        # WAM links / joints
        self.base_link_frame = 'wam/base_link'
        self.forearm_link_frame = 'wam/forearm_link'
        self.ee_link_frame = 'wam/wrist_palm_link' # 'wam/bhand/bhand_grasp_link'
        self.zed_link_frame = 'zed_camera_center'  # 'zed_camera_center' or 'camera_frame'
        self.camera_link_frame = 'camera_frame'
        self.object_frame = 'object_frame'

        # Transforms
        self.tf_buffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tf_buffer)
        self.transform_broadcaster = tf2_ros.TransformBroadcaster()

        #######################
        # ZED Measurements
        #######################

        self.offset_to_left_ZED_lens_x_axis = -135 / 1000    # -87.5 / 1000 # zed height
        self.offset_to_left_ZED_lens_y_axis = -150 / 1000    # -187.5 / 1000  # TODO: GET A MEASUREMENT !!!
        self.offset_to_left_ZED_lens_z_axis = 65 / 1000      # left lens

        #######################
        # Transforms: ZED tf tree
        #######################

        self.zed_frame = geometry_msgs.msg.TransformStamped()
        self.zed_frame.header.frame_id = self.forearm_link_frame
        self.zed_frame.child_frame_id = self.zed_link_frame

        self.zed_in_forearm_frame = PoseStamped()
        self.zed_in_forearm_frame.header.stamp = rospy.Time.now()
        self.zed_in_forearm_frame.header.frame_id = self.base_link_frame
        self.zed_in_forearm_frame.pose.position.x = self.offset_to_left_ZED_lens_x_axis
        self.zed_in_forearm_frame.pose.position.y = self.offset_to_left_ZED_lens_y_axis
        self.zed_in_forearm_frame.pose.position.z = 0 # self.offset_to_left_ZED_lens_z_axis
        self.zed_in_forearm_frame.pose.orientation.w = 1 / np.sqrt(2)
        self.zed_in_forearm_frame.pose.orientation.x = 1 / np.sqrt(2)
        self.zed_in_forearm_frame.pose.orientation.y = 0
        self.zed_in_forearm_frame.pose.orientation.z = 0

        #######################
        # Transforms: DUMMY CAMERA LINK FOR OBJECT TRANSFORMS
        #######################

        self.camera_frame = geometry_msgs.msg.TransformStamped()
        self.camera_frame.header.frame_id = self.forearm_link_frame # self.base_link_frame
        self.camera_frame.child_frame_id = self.camera_link_frame

        self.camera_in_forearm_frame = PoseStamped()
        self.camera_in_forearm_frame.header.frame_id = self.base_link_frame  # self.camera_frame or self.base_frame
        self.camera_in_forearm_frame.pose.position.x = self.offset_to_left_ZED_lens_x_axis
        self.camera_in_forearm_frame.pose.position.y = self.offset_to_left_ZED_lens_y_axis
        self.camera_in_forearm_frame.pose.position.z = self.offset_to_left_ZED_lens_z_axis
        self.camera_in_forearm_frame.pose.orientation.w = 0.5 # 0 # 0.5
        self.camera_in_forearm_frame.pose.orientation.x = 0.5 # 0 # 0.5
        self.camera_in_forearm_frame.pose.orientation.y = 0.5 # -1 / np.sqrt(2) # 0.5
        self.camera_in_forearm_frame.pose.orientation.z = -0.5 # 1 / np.sqrt(2) # -0.5

        #######################
        # PLY FILE
        #######################
        self.zed_wrist_mount = self.load_obj_ply_files()
        # print("self.zed_wrist_mount: ", len(self.zed_wrist_mount))

        self.pub_zed_wrist_mount = rospy.Publisher('zed_wrist_mount', PointCloud2, queue_size=1)

        #######################
        # JointState Callbacks
        #######################
        self.joint_states_sub = rospy.Subscriber("/wam/joint_states", JointState, self.get_zed_in_world_transform)

    #######################
    #######################

    def get_zed_in_world_transform(self, data):
        ''' zed_T_world = zed_T_forearm * forearm_T_world '''

        #######################
        # ZED CENTER
        #######################

        self.zed_frame.header.stamp = rospy.Time.now()
        self.zed_frame.transform.translation.x = self.zed_in_forearm_frame.pose.position.x
        self.zed_frame.transform.translation.y = self.zed_in_forearm_frame.pose.position.y
        self.zed_frame.transform.translation.z = self.zed_in_forearm_frame.pose.position.z
        self.zed_frame.transform.rotation.x = self.zed_in_forearm_frame.pose.orientation.x
        self.zed_frame.transform.rotation.y = self.zed_in_forearm_frame.pose.orientation.y
        self.zed_frame.transform.rotation.z = self.zed_in_forearm_frame.pose.orientation.z
        self.zed_frame.transform.rotation.w = self.zed_in_forearm_frame.pose.orientation.w

        self.transform_broadcaster.sendTransform(self.zed_frame)

        #######################
        # DUMMY CAMERA LINK FOR OBJECT TRANSFORMS
        #######################

        self.camera_frame.header.stamp = rospy.Time.now()
        self.camera_frame.transform.translation.x = self.camera_in_forearm_frame.pose.position.x
        self.camera_frame.transform.translation.y = self.camera_in_forearm_frame.pose.position.y
        self.camera_frame.transform.translation.z = self.camera_in_forearm_frame.pose.position.z
        self.camera_frame.transform.rotation.x = self.camera_in_forearm_frame.pose.orientation.x
        self.camera_frame.transform.rotation.y = self.camera_in_forearm_frame.pose.orientation.y
        self.camera_frame.transform.rotation.z = self.camera_in_forearm_frame.pose.orientation.z
        self.camera_frame.transform.rotation.w = self.camera_in_forearm_frame.pose.orientation.w

        self.transform_broadcaster.sendTransform(self.camera_frame)

        #######################
        # Pub Camera Mount
        #######################

        t = np.array([0, 0, 0]).reshape(-1)
        q = np.array([0, 0, 0, 1]).reshape(-1)
        q = self.modify_obj_rotation_matrix_for_grasping(q)
        r = np.array(R.from_quat(q).as_dcm()).reshape(3, 3)

        # t = np.array([self.camera_frame.transform.translation.x,
        #               self.camera_frame.transform.translation.y,
        #               self.camera_frame.transform.translation.z]).reshape(-1)
        # q = np.array([self.camera_frame.transform.rotation.x,
        #               self.camera_frame.transform.rotation.y,
        #               self.camera_frame.transform.rotation.z,
        #               self.camera_frame.transform.rotation.w]).reshape(-1)
        # r = np.array(R.from_quat(q).as_dcm()).reshape(3, 3)

        # print("t:{}".format(t))
        # print("q:{}".format(q))
        # print("r:{}".format(r))

        # pointcloud
        header = std_msgs.msg.Header()
        header.stamp = rospy.Time.now()
        header.frame_id = self.camera_link_frame # '/world' # self.zed_link_frame
        _model_points = np.dot(self.zed_wrist_mount, r.T) + t
        model_points = pcl2.create_cloud_xyz32(header, _model_points)
        self.pub_zed_wrist_mount.publish(model_points)

    #######################
    #######################

    def load_obj_ply_files(self):

        ###################################
        # ZED WRIST MOUNT PLY
        ###################################

        rospy.loginfo("")
        rospy.loginfo("*** Loading Mesh file ***")
        input_file = open('/home/akeaveny/catkin_ws/src/ARLBarretWamArm/barrett_tf_publisher/meshes/ZED_wrist_mount/zed_wrist_mount.xyz')

        zed_wrist_mount = []
        while 1:
            input_line = input_file.readline()
            if not input_line:
                break
            input_line = input_line[:-1].split(' ')
            zed_wrist_mount.append([float(input_line[0]), float(input_line[1]), float(input_line[2])])
        zed_wrist_mount = np.array(zed_wrist_mount)
        input_file.close()

        rospy.loginfo("Loaded ZED Wrist Mount")
        rospy.loginfo("")
        return zed_wrist_mount

    #######################
    #######################

    def modify_obj_rotation_matrix_for_grasping(self, q):

        #######################
        #######################

        # theta = np.pi/2
        # ccw_x_rotation = np.array([[1, 0, 0],
        #                            [0, np.cos(theta), -np.sin(theta)],
        #                            [0, np.sin(theta), np.cos(theta)],
        #                            ])
        #
        # ccw_y_rotation = np.array([[np.cos(theta), 0 , np.sin(theta)],
        #                            [0, 1, 0],
        #                            [-np.sin(theta), 0, np.cos(theta)],
        #                            ])
        #
        # ccw_z_rotation = np.array([[np.cos(theta), -np.sin(theta), 0],
        #                            [np.sin(theta), np.cos(theta), 0],
        #                            [0, 0, 1],
        #                            ])

        #######################
        #######################

        rot = np.asarray(R.from_quat(q).as_dcm()).reshape(3, 3)

        # rotate about x-axis
        theta = np.pi/2
        ccw_x_rotation = np.array([[1, 0, 0],
                                   [0, np.cos(theta), -np.sin(theta)],
                                   [0, np.sin(theta), np.cos(theta)],
                                   ])
        rot = np.dot(rot, ccw_x_rotation)

        # rotate about z-axis
        # theta = np.pi
        # ccw_z_rotation = np.array([[np.cos(theta), -np.sin(theta), 0],
        #                            [np.sin(theta), np.cos(theta), 0],
        #                            [0, 0, 1],
        #                            ])
        # rot = np.dot(rot, ccw_z_rotation)

        #######################
        #######################

        return np.asarray(R.from_dcm(rot).as_quat()).reshape(-1)

    #######################
    #######################

def main():

    rospy.init_node('barrett_tf_publisher', anonymous=True)
    TFPublisher()
    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        rate.sleep()
    # try:
    #     rospy.spin()
    # except KeyboardInterrupt:
    #     print ('Shutting down Moveit')

if __name__ == '__main__':
    main()
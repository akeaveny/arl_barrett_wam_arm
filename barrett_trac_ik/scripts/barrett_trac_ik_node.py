#! /usr/bin/env python
from __future__ import division

import sys
import copy

import numpy as np
import cv2

from pyquaternion import Quaternion
from scipy.spatial.transform import Rotation as R

import rospy

import tf
import tf2_ros
import tf2_geometry_msgs
import geometry_msgs.msg

from geometry_msgs.msg import Pose, PoseStamped
from std_msgs.msg import Bool

from trac_ik_python.trac_ik import IK

#######################
#######################

class TracIKPublisher():

    def __init__(self):

        ##################################
        # TF transforms
        ##################################

        # Transforms
        self.tf_buffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tf_buffer)
        self.transform_broadcaster = tf2_ros.TransformBroadcaster()

        # WAM links / joints
        self.base_link_frame = 'wam/base_link'
        self.forearm_link_frame = 'wam/forearm_link'
        self.ee_link_frame = 'wam/bhand/bhand_grasp_link'  # 'wam/bhand/bhand_grasp_link'
        self.zed_link_frame = 'zed_camera_center'          # 'zed_camera_center' or 'camera_frame'
        self.camera_link_frame = 'camera_frame'
        self.object_frame = 'object_frame'
        self.grasp_frame = 'grasp_frame'

        #######################
        # Object Pose
        #######################

        # self.object_pose_sub = rospy.Subscriber("/aruco_single/pose", PoseStamped, self.object_pose_callback)
        self.object_pose_sub = rospy.Subscriber("/arl_vicon_ros/aff_densefusion_pose", PoseStamped, self.object_pose_callback)
        # self.object_pose_sub = rospy.Subscriber("/arl_affpose_ros/aff_densefusion_pose", PoseStamped, self.object_pose_callback)

        #######################
        #######################

        # Trac IK
        self.ik_solver = IK(self.base_link_frame, self.ee_link_frame)
        # self.seed_state = [0.00] * self.ik_solver.number_of_joints
        self.seed_state = [0.00010957005627754578, 0.8500968575130496, -0.00031928475261511213, 1.868559041954476, 0.0, -0.0006325693970662439, -0.00030823458564346445]

    #######################
    #######################

    def object_pose_callback(self, object_in_world_frame_msg):

        #######################
        #######################

        x = object_in_world_frame_msg.pose.position.x
        y = object_in_world_frame_msg.pose.position.y
        z = object_in_world_frame_msg.pose.position.z
        position = np.array([x, y, z])

        w = object_in_world_frame_msg.pose.orientation.w
        x = object_in_world_frame_msg.pose.orientation.x
        y = object_in_world_frame_msg.pose.orientation.y
        z = object_in_world_frame_msg.pose.orientation.z
        orientation = np.array([x, y, z, w])

        #######################
        # modify for gripper
        #######################

        position = np.array([0.75, 0, 0.25])
        position[2] = position[2] + 5/100    # offset for: 'wam/bhand/bhand_grasp_link'
        # position[2] = position[2] + 15/100 # offset for: 'wam/wrist_palm_link'

        obj_id = 5 # todo
        orientation = self.modify_obj_rotation_matrix_for_grasping(obj_id=obj_id, q=orientation)

        print('')
        self.print_object_pose(q=orientation, t=position)

        #######################
        # pub tf
        #######################

        # tf
        grasp_frame_on_object = geometry_msgs.msg.TransformStamped()
        grasp_frame_on_object.header.frame_id = self.base_link_frame
        grasp_frame_on_object.child_frame_id = self.grasp_frame
        grasp_frame_on_object.header.stamp = rospy.Time.now()
        grasp_frame_on_object.transform.translation.x = position[0]
        grasp_frame_on_object.transform.translation.y = position[1]
        grasp_frame_on_object.transform.translation.z = position[2]
        grasp_frame_on_object.transform.rotation.x = orientation[0]
        grasp_frame_on_object.transform.rotation.y = orientation[1]
        grasp_frame_on_object.transform.rotation.z = orientation[2]
        grasp_frame_on_object.transform.rotation.w = orientation[3]
        self.transform_broadcaster.sendTransform(grasp_frame_on_object)

        #######################
        # Trac IK
        #######################

        wam_joint_states = self.ik_solver.get_ik(self.seed_state,
                                                #########################
                                                position[0], position[1], position[2]+10/100,
                                                orientation[0], orientation[1], orientation[2], orientation[3],
                                                #########################
                                                # brx=0.5, bry=0.5, brz=0.5
                                                )

        print("[trac-ik]     /wam/joint_states: ", wam_joint_states)

        wam_joint_states = self.ik_solver.get_ik(self.seed_state,
                                                #########################
                                                position[0], position[1], position[2],
                                                orientation[0], orientation[1], orientation[2], orientation[3],
                                                #########################
                                                # brx=0.5, bry=0.5, brz=0.5
                                                )

        print("[trac-ik]     /wam/joint_states: ", wam_joint_states)


    #######################
    #######################


    def print_object_pose(self, q, t):

        # convert translation to [cm]
        t = t.copy() * 100

        rot = np.array(R.from_quat(q).as_dcm()).reshape(3, 3)
        rvec, _ = cv2.Rodrigues(rot)
        rvec = rvec * 180 / np.pi
        rvec = np.squeeze(np.array(rvec)).reshape(-1)

        rospy.loginfo('position    [cm]:  x:{:.2f}, y:{:.2f}, z:{:.2f}'.format(t[0], t[1], t[2]))
        rospy.loginfo('orientation [deg]: x:{:.2f}, y:{:.2f}, z:{:.2f}'.format(rvec[0], rvec[1], rvec[2]))

    #######################
    #######################

    def modify_obj_rotation_matrix_for_grasping(self, obj_id, q):

        #######################
        #######################

        # theta = np.pi/2
        # ccw_x_rotation = np.array([[1, 0, 0],
        #                            [0, np.cos(theta), -np.sin(theta)],
        #                            [0, np.sin(theta), np.cos(theta)],
        #                            ])
        #
        # ccw_y_rotation = np.array([[np.cos(theta), 0 , np.sin(theta)],
        #                            [0, 1, 0],
        #                            [-np.sin(theta), 0, np.cos(theta)],
        #                            ])
        #
        # ccw_z_rotation = np.array([[np.cos(theta), -np.sin(theta), 0],
        #                            [np.sin(theta), np.cos(theta), 0],
        #                            [0, 0, 1],
        #                            ])

        #######################
        #######################

        rot = np.asarray(R.from_quat(q).as_dcm()).reshape(3, 3)

        #######################
        #######################

        if obj_id in np.array([1, 5]):

            # rotate about z-axis
            theta = np.pi / 2
            ccw_z_rotation = np.array([[np.cos(theta), -np.sin(theta), 0],
                                       [np.sin(theta), np.cos(theta), 0],
                                       [0, 0, 1],
                                       ])
            rot = np.dot(rot, ccw_z_rotation)

            # rotate about x-axis
            theta = np.pi
            ccw_x_rotation = np.array([[1, 0, 0],
                                       [0, np.cos(theta), -np.sin(theta)],
                                       [0, np.sin(theta), np.cos(theta)],
                                       ])
            rot = np.dot(rot, ccw_x_rotation)

            # rotate about y-axis
            theta = np.pi / 2
            ccw_y_rotation = np.array([[np.cos(theta), 0 , np.sin(theta)],
                                       [0, 1, 0],
                                       [-np.sin(theta), 0, np.cos(theta)],
                                       ])
            rot = np.dot(rot, ccw_y_rotation)

        #######################
        #######################

        return np.asarray(R.from_dcm(rot).as_quat()).reshape(-1)

    #######################
    #######################

def main():

    rospy.init_node('barrett_trac_ik', anonymous=True)
    TracIKPublisher()
    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == '__main__':
    main()